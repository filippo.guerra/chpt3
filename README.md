Supplementary Tables for Chapter 3 of the thesis "The role of doublesex in shaping temporal, spatial and species-specific sex differentiation in Nasonia wasps"

Supplementary Table 1: Primers used in qPCR-mediated gene expression analysis. The temperature used in the annealing step is reported in the last column "T (ºC)".

Supplementary Table 2: Primers used in the synthesis of dsRNA template for RNAi. All primers have a version bearing an SP6 promoter at their 5’ end and one that does not.

Supplementary Table 3: Primers used in the assembly of plasmid for in-vitro production of DSX proteins.

Supplementary Table 4: Primers designed to produce the amplicon library used in DAP-qPCR and subsequent qPCR querying. 
